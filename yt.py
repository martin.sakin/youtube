#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Date : 2018-11-26 14:33:03
# @Author : Martin Sakin, martin.sakin <at> gmail.com
# Download mp3 or mp4 from youtube.
# You need ffmpeg and youtube-dl # sudo -H pip3 install youtube-dl

import subprocess
import os
import sys
import json
from datetime import datetime

TMP = './new/'
LOG_FILE = "downloaded.log"


def log(title, link):
	current_time = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
	with open(LOG_FILE, "a+") as f:
		f.write(current_time + "\n" + title + "\n" + link + "\n\n")


def select_best_audio(info):
	best_tag = '0'
	best_abr = 0

	for f in info['formats']:
		if f['format_id'] == '22':
			return '22'
		if f['acodec'] == 'mp4a.40.2':
			if 'abr' in f and f['abr'] > best_abr:
				best_tag = f['format_id']
				best_abr = f['abr']

	if best_abr:
		return best_tag
	else:
		raise NameError('No useful audio')


def select_best_video(info):
	tag_22 = ''
	best_tag = ''
	best_format = 0

	for f in info['formats']:
		if f['format_id'] == '22':	# 720p A+V
			tag_22 = '22'

		if not f['ext'] == 'mp4':
			continue

		if f['vcodec'] == 'avc1.640028':	# 1080p
			return f['format_id']
		elif f['vcodec'] in ['avc1.4d401f', 'avc1.4d401e', 'avc1.4d4015', 'avc1.4d400d', 'avc1.4d400b']:
			if f['format_note'] == 'DASH video':
				continue
			elif int(f['format_note'].replace('p','')) > best_format:
				best_tag = f['format_id']
				best_format = int(f['format_note'].replace('p',''))

	if tag_22:
		return tag_22
	if best_format:
		return best_tag
	else:
		raise NameError('No useful video')


def get_info(source):
	cmd = ['youtube-dl', '-j', source]
	out = subprocess.check_output(cmd, stderr=subprocess.DEVNULL).decode("utf-8")
	info = json.loads(out)
	return info


def title_name(info):
	return info['title'].replace('"', '').replace('/',',').replace('&', 'and').replace(':','-').replace('?','')


def download_music(info, source):
	tag = select_best_audio(info)
	title = title_name(info)
	title = title.replace('!','')
	print("AUDIO", tag, title)
	log(title, source)

	name_audio = TMP + title
	name_audio_out = TMP + title + '.mp3'

	cmd = ['youtube-dl', '-f', tag, '-o', name_audio, source]
	subprocess.run(cmd, stderr=subprocess.DEVNULL)

	cmd = ['ffmpeg', '-i', name_audio, '-vn', '-ab', '320k', '-ar', '44100', '-y', name_audio_out]
	subprocess.run(cmd, stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)

	os.remove(name_audio)
	return name_audio_out


def download_video(info, source):
	try:
		tag = select_best_video(info)
	except NameError:
		return ""
	title = title_name(info)
	title = title.replace('!','')
	print("VIDEO", tag, title)
	log(title, source)

	if tag == '22':
		cmd = ['youtube-dl', '-f', tag, source]
		subprocess.run(cmd, stderr=subprocess.DEVNULL)
		return

	name_audio = download_music(info, source)

	name_video = TMP + title
	name_video_out = TMP + title + ".mp4"
	#print(" name_audio =", name_audio, "\n name_video =", name_video, "\n name_video_out =",name_video_out)
	cmd = ['youtube-dl', '-f', tag, '-o', name_video, source]
	subprocess.run(cmd, stderr=subprocess.DEVNULL)

	cmd = ['ffmpeg', '-i', name_video, '-i', name_audio, '-acodec', 'copy', '-vcodec', 'copy', '-strict', '-2', name_video_out]
	print(' '.join(cmd))
	subprocess.run(cmd, stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)

	os.remove(name_video)
	os.remove(name_audio)
	return name_video_out


def download(mpx, source):
	try:
		info = get_info(source)
	except:
		print("Cannot get info from source")
		return 1

	if mpx == 'mp3':
		download_music(info, source)
	elif mpx == 'mp4':
		download_video(info, source)


def from_file(mpx, filename):
	try:
		with open(filename, 'r') as f:
			for url in f:
				url = url.strip()
				if not url or url.startswith('#') or url.startswith(';'):
					continue
				download(mpx, url)
				print("-----------------------------------------------")
	except FileNotFoundError:
		print("File '" + filename + "' not found!")
		exit(3)


def print_help():
	print("HELP:")
	print(' ' + sys.argv[0] + ' (mp3|mp4) https://www.youtube.com/watch?v=XXXXXXXXXXX')
	print(' ' + sys.argv[0] + ' (mp3|mp4) file_with_urls.x')
	print(' ' + sys.argv[0] + ' (mp3|mp4) XXXXXXXXXXX')


if __name__ == '__main__':
	if len(sys.argv) == 3:
		mpx = sys.argv[1]
		source = sys.argv[2]

		if not mpx in ['mp3','mp4', '3', '4']:
			print_help()
			exit(1)
		elif mpx in ['3', '4']:
				mpx = 'mp' + mpx

		try: os.makedirs(TMP)
		except: pass

		if source.find('youtube.com/') > -1:	# full link
			download(mpx, source)
		elif source.find('.') > -1:	# file with full links
			from_file(mpx, source)
		else:
			download(mpx, "https://www.youtube.com/watch?v=" + source)
	else:
		print_help()
		exit(0)
