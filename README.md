# Youtube music downloader

This script downloads video from youtube and convert to mp3.
It is wrote in Python3 and it is a wrapping extension of the youtube_dl tool to make download easier.

Last update: 2021-05-12

### Prerequisites

Library youtube_dl: https://pypi.org/project/youtube_dl/ for download video
and ffmpeg tool: https://www.ffmpeg.org/ to convert video to mp3.

	pip3 install youtube-dl
	sudo apt install -y ffmpeg

## Download video/audio

**Print help**: You can print help just by running a script without any arguments:

	python3 ./yt.py

**First argument**: select mp4 or mp3

**Second argument**: source of youtube video, it can be all link of just ID of video

**Download video** - the same result

	python3 ./yt.py mp4 https://www.youtube.com/watch?v=XXXXXXXXXXX
	python3 ./yt.py 4 https://www.youtube.com/watch?v=XXXXXXXXXXX
	python3 ./yt.py 4 XXXXXXXXXXX

**Download music** - the same result, download video and convert to mp3

	python3 ./yt.py mp3 https://www.youtube.com/watch?v=XXXXXXXXXXX
	python3 ./yt.py 3 https://www.youtube.com/watch?v=XXXXXXXXXXX
	python3 ./yt.py 3 XXXXXXXXXXX


---
